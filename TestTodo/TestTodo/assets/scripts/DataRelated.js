﻿/// <reference path="jquery-3.2.1.js" />


$(document).ready(function () {

    // Getting All ListItem Logic
    // ==========================

    function getData() {

        var ulList = $("#list");
        $.ajax({
            type: 'GET',
            url: "http://localhost:49345/api/items",
            dataType: 'json',
            success: function (data) {
                $.each(data, function (index, val) {
                    var html = itemTemplate.replace("[Desc]", val.Description);
                    //html = itemTemplate.replace()
                    ulList.append(html);
                });
            }
        });
    }

    getData();

    // Add New ListItem Logic
    // ======================

    $("#btnAdd").click(function () {

        var ListItem = new Object();
        ListItem.Description = $("#txtUserInput").val();
        ListItem.IsDone = 0;
        ListItem.ItemPosition = $("ul li").length + 1;
        ListItem.ListColor = 'red',
        ListItem.CreateDate = new Date().toString();


        $.ajax({
            url: "http://localhost:49345/api/items/post",
            method: "POST",
            contentType: "application/json",
            Accept: "text/html",
            data: { value: ListItem },
            success: function () {
                alert("Data Inserted");
            }
        });
    });

    
    // List Template
    //==============

    var itemTemplate = "<li id='lilist' color='1' class='colorBlue' rel='1' id='2'><span class='[dbColor]' id='Description' title='Double-click to edit...' style='opacity: 1;'>[Desc]</span>";
    itemTemplate += "<div class='draggertab tab'></div><input type='color' id='colorTab' style='border: none' class='colortab tab' /><div id='Color' runat='server' class='colortab tab'></div>";
    itemTemplate += "<div id='deleteTab' runat='server' class='deletetab tab' style='width: 44px; display: block; right: -64px;'></div>";
    itemTemplate += "<div id='IsDone' runat='server' class='donetab tab'></li>";


     //WebService Call To Save Data Using asmx
     //=======================================

    //function Add() {
    //    var userInput = document.getElementById("txtUserInput").value;
    //    TestTodo.Data.Add(userInput, AddSuccessCallBack, AddFailedCallBack);

    //    function AddSuccessCallBack() {
    //        getData();
    //    }

    //    function AddFailedCallBack() {
    //        alert("data failed");
    //    }
    //}
});