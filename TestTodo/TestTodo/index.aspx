﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="TestTodo.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
    <link rel="stylesheet" href="assets/style.css" type="text/css" />
    <script src="assets/scripts/jquery-3.2.1.js"></script>
    <script src="assets/scripts/DataRelated.js"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="page-wrap">
            <div id="header">
                <h1><a href="#">TestTodo</a></h1>
            </div>

            <div id="main">
                <ul id="list" class="ui-sortable">
                </ul>
                <br />
                <input id="txtUserInput" type="text" style="float:left" />
                <input id="btnAdd" class="" type="button" value="Add"/>
<%--                <asp:TextBox ID="txtUserInput" runat="server"></asp:TextBox>
                <asp:Button ID="btnAdd" CssClass="button" runat="server" Text="Add" OnClick="btnAdd_Click" />--%>
            </div>
        </div>
    </form>
</body>
</html>
