﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using TestTodo.Models;
using System.Web.Script.Serialization;

namespace TestTodo
{
    /// <summary>
    /// Summary description for Data
    /// </summary>
    [WebService(Namespace = "http://ashar.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Data : System.Web.Services.WebService
    {

        [WebMethod]
        public void Add(string userInput)
        {
            ListItem listItem = new ListItem();
            listItem.Add(userInput);
        }
    }
}
