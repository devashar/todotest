﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace TestTodo.Models
{
    public class ListItem
    {
        public int ListItemId { get; set; }
        public string Description { get; set; }
        public int IsDone { get; set; }
        public int ItemPosition { get; set; }
        public string ListColor { get; set; }
        public string CreateDate { get; set; }

        public List<ListItem> GetAllListItems()
        {
            List<ListItem> items = new List<ListItem>();
            string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("spGetAllItems", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ListItem item = new ListItem();
                    item.Description = rdr["Description"].ToString();
                    item.IsDone = Convert.ToInt32(rdr["IsDone"]);
                    item.ItemPosition = Convert.ToInt32(rdr["ItemPostition"]);
                    item.ListColor = rdr["ListColor"].ToString();
                    item.CreateDate = rdr["CreateDate"].ToString();

                    items.Add(item);
                }
            }

            return items;
        }

        public int GetItemPostion()
        {
            int LastId;
            string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("SELECT Top 1 ItemPostition From ListItems Order By ItemPostition DESC", con);
                con.Open();
                LastId = Convert.ToInt32(cmd.ExecuteScalar());
            }

            return LastId + 1;
        }

        public void Add(string _userInput)
        {
            if (!string.IsNullOrEmpty(_userInput))
            {
                string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("spAddItem", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Description", _userInput);
                    cmd.Parameters.AddWithValue("@ItemPostion", GetItemPostion());
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}