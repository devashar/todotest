﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BusinessLayer.Controllers
{
    //[RoutePrefix("api/Items")]
    public class ItemsController : ApiController
    {
        [HttpGet]
        public IEnumerable<ListItem> Get()
        {
            using (ItemDBContext db = new ItemDBContext())
            {
                return db.ListItems.ToList();
            }
        }

        //[HttpPost]
        public void Post([FromBody]ListItem value)
        {
            using (ItemDBContext db = new ItemDBContext())
            {
                db.ListItems.Add(value);
            }
        }
    }
}
